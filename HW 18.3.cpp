#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

struct Node {
	string data;
	Node* next;
};

class Stack {
public:
	Stack();
	~Stack();
	void push(string d);
	string pop();
	string Show();
	bool isEmpty();

	private:
		Node *top;
};

Stack::Stack()
{
	top = NULL;
}

Stack::~Stack()
{
	while (!isEmpty())
		pop();


	/*if (top == NULL)
	{
		cout << "nothing to clean up" << endl;
	}
	else
	{
		cout << "delete should be happening now" << endl;
	}*/
}

void Stack::push(string d) 
{
	Node* temp = new Node;
	temp->data = d;
	temp->next = top;
	top = temp;
}

string Stack::pop()
{
	
		string value = top->data;
		Node *oldtop = top;
		top = oldtop->next;
		delete oldtop;
		return value;
	
}

string Stack::Show()
{
	string result = "(top) -> ";
	if (isEmpty())
	{
		result = result + "NULL";
		return result;
	 }
	else 
	{
		Node* current = top;
		while (current != NULL)
		{
			result = result + current->data + " -> ";
		current = current->next;
		}
		result = result + "(END)";
		return result;
	}

}

bool Stack::isEmpty()
{
	return (top == NULL);
}

int main() 
{
	Stack* s = new Stack();
	cout << "Output when Empty: " << endl << s->Show() << endl;
	s->push("Chesseburger");
	s->push("Coffee");
	s->push("Pizza");
	s->pop();
	cout << "Output when not Empty: " << endl << s->Show() << endl;

	delete s;
return 0;
}